# README #

This repo configures my previous architecture in the following way:

* Installs and configures Haproxy as a loadbalancer on the proxy machine
* Installs and configures nginx on the two webservers machines
* Installs and configures postgresql on the database machine
* Installs and configures nginx and postgresql on the development machine

###Configuring the Management machine###
The first task is to correct our inventory with the right private ips that we get from AWS. We do that by vi inventory.
Then we have to configure Jenkins on the Management machine by adding 
deb https://pkg.jenkins.io/debian-stable binary/ to the 
/etc/apt/sources.list file. To complete Jenkins installation we execute the following:

```
#!bash

sudo apt-get update
sudo apt-get install jenkins
```
Now if we go to our browser on the 8080 port of the Elastic Ip given to us from the initial ansible play from the previous repo, we can see Jenkins running. So we can configure Jenkins by following the on screen details.

To provision everything on the nodes of the production environment we have to run simply ansible-playbook site.yml because we have a modified ansible.cfg file in the same directory. So ansible by default looks there for configuration.